import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.phytech.database.DBConnectionPoolManager;
import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.database.EnumDBType;
import com.phytech.model.Sensors.SensorEx;
import com.phytech.utils.Constants;
import com.phytech.utils.PropertiesManager;

public class logsParser {

	private static final Logger log = Logger.getLogger(logsParser.class);
	private static String bucketName = "logs.phytech-gate-production-us";
	private static String downloadPath = "../";
	private static String specificDates = null;
	private static int removeMessagesOlderThan = 15;

	private static boolean DEBUG = false;

	public static void main(String[] args) {
		log.info("Start...");
		initProperties();
		PropertiesManager.initConfiguration(System.getenv(Constants.CONFIGURATION_FILE_VARIABLE_NAME));
		updateCredentials();

		DBConnectionPoolManager.createPoolForDB(EnumDBType.JAVA);

		if (DEBUG) {
			test();
			return;
		}

		int day;
		int month;
		int year;

		if (specificDates == null) {

			long now = System.currentTimeMillis();
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			cal.setTimeInMillis(now);

			cal.add(Calendar.DATE, -1);

			day = cal.get(Calendar.DAY_OF_MONTH);
			month = cal.get(Calendar.MONTH);
			month++;
			year = cal.get(Calendar.YEAR);

			cal.add(Calendar.DATE, removeMessagesOlderThan * -1);
			long old = cal.getTimeInMillis();

			DBQueriesManagerSensors.removeSensorMeasurements(old);
			DBQueriesManagerSensors.removeLoggerData(old);

			log.info("Search for log files from " + day + "-" + month + "-" + year + "[UTC]");
			try {
				downloadFromS3(day, month, year);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			String[] dates = specificDates.split(";");
			for (String specificDate : dates) {
				String[] parts = specificDate.trim().split("-");
				day = Integer.parseInt(parts[0]);
				month = Integer.parseInt(parts[1]);
				year = Integer.parseInt(parts[2]);
				log.info("Search for log files from " + day + "-" + month + "-" + year + "[UTC]");
				try {
					downloadFromS3(day, month, year);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

	}

	private static void test() {
		// String testString = "2018-06-11T01:04:22+00:00,Info,\"SensorData - Id: 73279
		// TypeID: 23 Interval: 3600000msec loggerID: 73276 min threshold: -32768 max
		// threshold: 32767 arm min threshold: -32768 arm max threshold: 32767
		// Timestamp: 10/6/2018 2:00:00 Values: {
		// 34,35,35,34,33,33,34,32,31,30,30,30,30,30,30,30,30,30,30,31,32,33,33,34
		// }\",phytech-gate-production,web,app,,,,1,";
		String testString = "2018-06-19 03:11:54 INFO  logsParser:246 - [x]2018-06-18T21:15:48+00:00,Info,\"SensorData - Id: 536311 TypeID: 71 Interval: 3600000msec loggerID: 536063 latitude: 0.0 longitude: 0.0 clientId: 0 RSSI: 18 battery: 52152 Timestamp: 5/7/2018 10:00:00 Values: { 136,144,149 }\",phytech-gate-production,web,app,,,,1,";

		SensorEx sensor = new SensorEx(testString);

		List<SensorEx> aa = new ArrayList<>();
		aa.add(sensor);
		DBQueriesManagerSensors.insertSensorMeasurements(aa);

		testString = "2018-07-30 06:22:05 [pool-1-thread-153] DEBUG RabbitMQConnector:53 - sent to logger q: {\"logger_id\":67816,\"access_point\":\"internet\",\"rssi\":9,\"uncertainty_radius\":2500,\"mobile_net_code\":\"01\",\"sim_voice\":\"********************\",\"timezone\":\"UTC\",\"latitude\":32.25776,\"dl_interval\":1,\"use_country_code\":false,\"mobile_country_code\":\"425\",\"version\":\"S.10.116.5\",\"roaming_delay\":15,\"port\":1018,\"dl_cycles\":13,\"start_dl\":1,\"batt_volt\":7533,\"mobile_cid\":\"0597*\",\"server_url\":\"proxy.backup.phytech.com\",\"epoch_clock\":1532931660000,\"longitude\":34.856483}";

		com.phytech.model.Logger lg = com.phytech.model.Logger
				.fromString(testString.substring(testString.indexOf("{")));

		List<com.phytech.model.Logger> loggers = new ArrayList<com.phytech.model.Logger>();
		loggers.add(lg);
		DBQueriesManagerSensors.insertLoggerMeasurements(loggers);
	}

	private static void initProperties() {
		if (System.getenv("DOWNLOAD_DIRECTORY") != null) {
			downloadPath = System.getenv("DOWNLOAD_DIRECTORY");
		}

		if (!(downloadPath.endsWith(File.separator) || downloadPath.endsWith("/")))
			downloadPath += File.separator;
		log.info("Download path = " + downloadPath);

		if (System.getenv("DEBUG") != null) {
			DEBUG = Boolean.parseBoolean(System.getenv("DEBUG"));
		}

		if (System.getenv("SPECIFIC_DATES") != null) {
			specificDates = System.getenv("SPECIFIC_DATES");
		}

		if (System.getenv("REMOVE_MESSAGES_OLDER_THAN") != null) {
			removeMessagesOlderThan = Integer.parseInt(System.getenv("REMOVE_MESSAGES_OLDER_THAN"));
		}
	}

	private static String prepareDateString(int day, int month, int year) {
		String logDate = year + "-";

		if (month < 10) {
			logDate += "0";
		}
		logDate += month + "-";
		if (day < 10)
			logDate += "0";
		logDate += day;

		return logDate;

	}

	public static void downloadFromS3(int day, int month, int year) throws IOException {
		String clientRegion = "us-east-1";
		String date = prepareDateString(day, month, year);

		S3Object fullObject = null, objectPortion = null, headerOverrideObject = null;
		try {
			AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(clientRegion)
					.withCredentials(new ProfileCredentialsProvider()).build();

			List<String> keys = getObjectslistFromFolder(s3Client, bucketName, "Coralogix/" + date);
			if (keys.isEmpty()) {
				log.info("Can't found");
				return;
			}
			int counter = 1;
			int sensorsCounter = 0;
			int loggersCounter = 0;
			for (String key : keys) {
				log.info("Downloading an object " + key);
				String archiveFileName = "Log" + counter + ".txt.gz";
				fullObject = s3Client.getObject(new GetObjectRequest(bucketName, key));
				S3ObjectInputStream inputStream = fullObject.getObjectContent();
				FileUtils.copyInputStreamToFile(inputStream, new File(downloadPath + File.separator + archiveFileName));

				// Unzip
				gUnzipFile(downloadPath + File.separator + archiveFileName, downloadPath + File.separator + "temp.txt");
				File file = new File(downloadPath + File.separator + "temp.txt");

				List<SensorEx> sensors = new ArrayList<SensorEx>();
				List<com.phytech.model.Logger> loggers = new ArrayList<com.phytech.model.Logger>();

				// Scan and search for patterns
				Scanner scanner = new Scanner(file);
				while (scanner.hasNextLine()) {
					boolean isLineOfInterestSensor = false;
					boolean isLineOfInterestLogger = false;

					final String currentLineFromFile = scanner.nextLine();
					isLineOfInterestSensor = isLineOfInterestSensor || currentLineFromFile.contains("SensorData - Id:");
					if (isLineOfInterestSensor) {
						SensorEx sensor = parseSensor(currentLineFromFile);
						sensors.add(sensor);

					}

					isLineOfInterestLogger = isLineOfInterestLogger
							|| currentLineFromFile.contains("sent to logger q:");
					if (isLineOfInterestLogger) {
						com.phytech.model.Logger logger = parseLogger(currentLineFromFile);
						if (logger != null) {
							loggers.add(logger);
						}

					}
				}
				scanner.close();

				// delete old files
				file.delete();

				File archive = new File(downloadPath + File.separator + archiveFileName);
				archive.delete();

				DBQueriesManagerSensors.insertSensorMeasurements(sensors);
				DBQueriesManagerSensors.insertLoggerMeasurements(loggers);

				sensorsCounter += sensors.size();
				loggersCounter += loggers.size();
				counter++;
			}

			log.info("Processed:");
			log.info("- " + counter + " log files");
			log.info("- " + sensorsCounter + " sensor messages");
			log.info("- " + loggersCounter + " logger parameters messages");

			log.info("------------------------------------------------------");

		} catch (Exception e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process
			// it, so it returned an error response.
			e.printStackTrace();
		} finally {
			// To ensure that the network connection doesn't remain open, close any open
			// input streams.
			if (fullObject != null) {
				fullObject.close();
			}
			if (objectPortion != null) {
				objectPortion.close();
			}
			if (headerOverrideObject != null) {
				headerOverrideObject.close();
			}
		}
	}

	public static List<String> getObjectslistFromFolder(AmazonS3 s3Client, String bucketName, String folderKey) {

		ListObjectsRequest listObjectsRequest = new ListObjectsRequest().withBucketName(bucketName)
				.withPrefix(folderKey + "/");

		List<String> keys = new ArrayList<>();

		ObjectListing objects = s3Client.listObjects(listObjectsRequest);
		for (;;) {
			List<S3ObjectSummary> summaries = objects.getObjectSummaries();
			if (summaries.size() < 1) {
				break;
			}
			summaries.forEach(s -> keys.add(s.getKey()));
			objects = s3Client.listNextBatchOfObjects(objects);
		}

		return keys;
	}

	public static void gUnzipFile(String inFileName, String outFileName) {

		byte[] buffer = new byte[1024];

		try {

			GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(inFileName));

			FileOutputStream out = new FileOutputStream(outFileName);

			int len;
			while ((len = gzis.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}

			gzis.close();
			out.close();

			System.out.println("UNZIP Done");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static SensorEx parseSensor(String sensorStr) {
		log.info("[x]" + sensorStr);
		SensorEx sensor = new SensorEx(sensorStr);
		return sensor;
	}

	private static com.phytech.model.Logger parseLogger(String currentLineFromFile) {
		String loggerStr = currentLineFromFile.substring(currentLineFromFile.indexOf("{"),
				currentLineFromFile.indexOf("}") + 1);
		loggerStr = loggerStr.replaceAll("\"\"", "\"");
		log.info("[xx]" + loggerStr);
		com.phytech.model.Logger logger = com.phytech.model.Logger.fromString(loggerStr);
		return logger;
	}

	private static void updateCredentials() {
		try {
			CredentialsProvider provider = new BasicCredentialsProvider();
			UsernamePasswordCredentials credentials = new UsernamePasswordCredentials("phytech", "123456");
			provider.setCredentials(AuthScope.ANY, credentials);

			HttpClient httpclient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

			HttpResponse response = httpclient.execute(new HttpGet(
					PropertiesManager.endpointURL + "/getDBCredentials?token=" + PropertiesManager.innerToken));
			log.info(response.getStatusLine().getStatusCode());
			String dbParams = EntityUtils.toString(response.getEntity());
			log.debug(" [x] Received '" + dbParams + "'");
			if (dbParams == null || dbParams.isEmpty() || dbParams.equals("Error")) {
				dbParams = System.getenv("DATABASE_URL");
			} else {
				log.debug("Got new DB credentials. Adding SSL info " + System.getenv("SSL_ADDITION"));
				dbParams += System.getenv("SSL_ADDITION");
			}

			PropertiesManager.updateDBandRabbit(dbParams, null);

		} catch (Exception e) {

		}

	}
}
