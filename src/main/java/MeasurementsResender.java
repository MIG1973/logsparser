
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.phytech.model.Sensors.Sensor;
import com.phytech.utils.Constants;
import com.phytech.utils.PropertiesManager;
import com.phytech.utils.QueueSender;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.BasicProperties.Builder;
import com.rabbitmq.client.MessageProperties;

/**
 * This program reads measurements from TZDispatcher's log file and re-send it
 * to Ruby's queue
 * 
 * 
 */
public class MeasurementsResender {

	private static final Logger log = LogManager.getLogger(MeasurementsResender.class);
	private static QueueSender sender = null;

	// for ruby
	//private static String rubyMeasurementsQueueName = "MEASUREMENTS";
	private static String javaMeasurementsQueueName = "MEASUREMENTS_JAVA";

	private static BasicProperties m_props;
	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final BasicProperties PERSIST_PROPERTY = MessageProperties.PERSISTENT_TEXT_PLAIN;
	private static boolean ANALYZE_MODE = false;

	public static void main(String[] args) {

		PropertiesManager.initConfiguration(System.getenv(Constants.CONFIGURATION_FILE_VARIABLE_NAME));

		AMQP.BasicProperties.Builder builder = new Builder();
		builder.contentType(JSON_CONTENT_TYPE);
		builder.deliveryMode(PERSIST_PROPERTY.getDeliveryMode());
		m_props = builder.build();

		//QueueSender.measurementsToSendparams.put("x-dead-letter-exchange", "dead_exchange");
		sender = new QueueSender(javaMeasurementsQueueName, QueueSender.measurementsToSendparams);

		if (System.getenv("ANALYZE_MODE") != null) {
			ANALYZE_MODE = Boolean.parseBoolean(System.getenv("ANALYZE_MODE"));
		}

		log.info("ANALYZE_MODE " + ANALYZE_MODE);
		ReadAndSendMeasurements();
	}

	private static void ReadAndSendMeasurements() {
		try {
			// read measurements from file
			String rootFolderName = System.getenv("LOG_FILE_PATH");
			File rootFolder = new File(rootFolderName);
			File[] logs = rootFolder.listFiles();
			if (logs == null) {
				return;
			}
			for (File logFile : logs) {
				// read file into stream, try-with-resources

				gUnzipFile(logFile.getPath(), "C:\\Temp" + File.separator + "temp.txt");
				System.out.println(logFile.getPath());
				File tmp = new File("C:\\Temp" + File.separator + "temp.txt");

				// Scan and search for patterns
				Scanner scanner = new Scanner(tmp);
				while (scanner.hasNextLine()) {
					boolean isLineOfInterest = false;
					final String currentLineFromFile = scanner.nextLine();
					isLineOfInterest = isLineOfInterest
							|| currentLineFromFile.contains("MeasurementsQueueConsumer:54 -  [x] Received");
					if (isLineOfInterest) {
						int messageStartIndex = currentLineFromFile.indexOf("[x] Received") + 14;
						int messageEndIndex = currentLineFromFile.indexOf("phytech-dispatcher-production") - 3;
						String message = currentLineFromFile.substring(messageStartIndex, messageEndIndex);
						String finalMessage = message.replace("\"\"", "\"");

						handleMessage(finalMessage);

					}

				}
				scanner.close();

			}

		} catch (Exception e) {
			log.error("ReadAndSendMeasurements error ", e);

		}
	}

	private static void handleMessage(String toSend) {
		try {

			if (sender.Send(toSend, m_props)) {
				log.info(toSend + "was succesfully sent to queue ");
			} else {
				log.error("Error sending " + toSend + " to queue ");
			}

		} catch (Exception ex) {
			log.error("Error handling message", ex);
		}
	}

	// if required add some logic here (for example look for message with
	// year=2001, or sensorId =XXXX
	public static boolean shouldSendSensor(Sensor sensor) {
		return true;
	}

	public static void gUnzipFile(String inFileName, String outFileName) {

		byte[] buffer = new byte[1024];

		try {

			GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(inFileName));

			FileOutputStream out = new FileOutputStream(outFileName);

			int len;
			while ((len = gzis.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}

			gzis.close();
			out.close();

			System.out.println("UNZIP Done");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
