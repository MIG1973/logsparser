cd lib

sudo mvn install:install-file -Dfile=PhytechCommon-1.1.jar -DgroupId=com.phytech -DartifactId=PhytechCommon  -Dversion=1.1 -Dpackaging=jar
sudo mvn deploy:deploy-file -Durl=file:../repo -Dfile=PhytechCommon-1.1.jar -DgroupId=com.phytech -DartifactId=PhytechCommon -Dpackaging=jar -Dversion=1.1

cd ..
sudo mvn install
sudo mvn install dependency:copy-dependencies
sudo chmod +x target/logsparser.sh
sudo mkdir ./target/resources
sudo cp src/main/resources/*.* ./target/resources

